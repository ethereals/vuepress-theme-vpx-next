# VPX - A theme developed based on VuePress

## 文档
查看我们的文档
GitHub ：https://qcyblm.github.io/vuepress-theme-vpx-next/
Gitee ：https://qcyblm.gitee.io/vuepress-theme-vpx-next/

```bash
yarn install
yarn build
yarn dev
yarn docs:dev
```

## LICENSE
[MIT](/LICENSE)